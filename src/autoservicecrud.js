/* eslint-disable no-restricted-syntax */

const del = require('./templateFunctions/del');
const insert = require('./templateFunctions/insert');
const select = require('./templateFunctions/select');
const update = require('./templateFunctions/update');

module.exports = (app, table, options = null) => {
  const functions = {
    del: del(app, table),
    insert: insert(app, table),
    select: select(app, table),
    update: update(app, table),
  }
  if (options) {
    for (const [functionName, customFunction] of options) {
      functions[functionName] = customFunction(app, table);
    }
  }
  return functions;
};

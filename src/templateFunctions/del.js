module.exports = (app, table) => idToBeDeleted => app.db(table).where(idToBeDeleted).del();

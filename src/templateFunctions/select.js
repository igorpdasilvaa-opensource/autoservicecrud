// eslint-disable-next-line max-len
module.exports = (app, table) => (filter = {}) => app.db(table).where(filter).select();

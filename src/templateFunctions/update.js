module.exports = (app, table) => (filter, objToBeUpdated) => app.db(table).where(filter).update(objToBeUpdated, 'id');

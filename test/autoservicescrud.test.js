const knex = require('knex');
const autoservicecrud = require('../src/autoservicecrud');

const appInstanceOfExpress = {};

beforeAll(async () => {
  appInstanceOfExpress.db = await knex({
    client: 'mysql',
    connection: {
      host: 'mysql',
      user: 'root',
      // password: 'rootpasswd',
      database: 'autoservicecrud',
    },
  });
});

test('should return the default crud pointing to table users', async () => {
  const usersService = autoservicecrud(appInstanceOfExpress, 'users');
  expect(usersService).toHaveProperty('del');
  expect(usersService).toHaveProperty('insert');
  expect(usersService).toHaveProperty('select');
  expect(usersService).toHaveProperty('update');
});

test('should return the custom select function', async () => {
  const usersService = autoservicecrud(appInstanceOfExpress, 'users', [
    ['select', (app, table) => () => 'i am a custom select function'],
  ]);

  expect(usersService).toHaveProperty('del');
  expect(usersService).toHaveProperty('insert');
  expect(usersService).toHaveProperty('select');
  expect(usersService).toHaveProperty('update');
  expect(usersService.select()).toBe('i am a custom select function');
});

test('should return the custom insert function', async () => {
  const usersService = autoservicecrud(appInstanceOfExpress, 'users', [
    ['insert', (app, table) => () => 'i am a custom insert function'],
  ]);
  expect(usersService).toHaveProperty('del');
  expect(usersService).toHaveProperty('insert');
  expect(usersService).toHaveProperty('select');
  expect(usersService).toHaveProperty('update');
  expect(usersService.insert()).toBe('i am a custom insert function');
});

test('should return the custom update function', async () => {
  const usersService = autoservicecrud(appInstanceOfExpress, 'users', [
    ['update', (app, table) => () => 'i am a custom update function'],
  ]);
  expect(usersService).toHaveProperty('del');
  expect(usersService).toHaveProperty('insert');
  expect(usersService).toHaveProperty('select');
  expect(usersService).toHaveProperty('update');
  expect(usersService.update()).toBe('i am a custom update function');
});

test('should return the custom del function', async () => {
  const usersService = autoservicecrud(appInstanceOfExpress, 'users', [
    ['del', (app, table) => () => 'i am a custom del function'],
  ]);
  expect(usersService).toHaveProperty('del');
  expect(usersService).toHaveProperty('insert');
  expect(usersService).toHaveProperty('select');
  expect(usersService).toHaveProperty('update');
  expect(usersService.del()).toBe('i am a custom del function');
});

test('should return all the custom functions', async () => {
  const usersService = autoservicecrud(appInstanceOfExpress, 'users', [
    ['del', (app, table) => () => 'i am a custom del function'],
    ['update', (app, table) => () => 'i am a custom update function'],
    ['insert', (app, table) => () => 'i am a custom insert function'],
    ['select', (app, table) => () => 'i am a custom select function'],
  ]);
  expect(usersService).toHaveProperty('del');
  expect(usersService).toHaveProperty('insert');
  expect(usersService).toHaveProperty('select');
  expect(usersService).toHaveProperty('update');
  expect(usersService.del()).toBe('i am a custom del function');
  expect(usersService.update()).toBe('i am a custom update function');
  expect(usersService.insert()).toBe('i am a custom insert function');
  expect(usersService.select()).toBe('i am a custom select function');
});

test('should return the default crud and a adicional custom function', async () => {
  const usersService = autoservicecrud(appInstanceOfExpress, 'users', [
    ['custom', (app, table) => () => 'i am a custom function'],
  ]);
  expect(usersService).toHaveProperty('del');
  expect(usersService).toHaveProperty('insert');
  expect(usersService).toHaveProperty('select');
  expect(usersService).toHaveProperty('update');
  expect(usersService.custom()).toBe('i am a custom function');
});
